﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    abstract class Item
    {
        public abstract void Cost();
        public abstract void Characteristic();
        public abstract void ShowCheapest();
        public abstract void ShowExpensive();
    }
    class Shop
    {
        public void Cost(Item item)
        {
            item.Cost();
        }
        public void Characteristic(Item item)
        {
            item.Characteristic();
        }

        public void ShowCheapest(Item item)
        {
            item.ShowCheapest();
            
        }
        public void ShowExpensive(Item item)
            {
            item.ShowExpensive();
            }
    }

    class Meat: Item
    {
        
        public override void Characteristic()
        {
            Console.WriteLine($"The MEAT is Eatable");
        }
        public override void Cost()
        {
            double m = 15;
            Console.WriteLine($"The MEAT costs {m} BYN");
        }

        public override void ShowCheapest()
        {
            Console.WriteLine();
        }

        public override void ShowExpensive()
        {
            Console.WriteLine( );
        }
    }

    class IceCream : Item
    {
        public override void Characteristic()
        {
            Console.WriteLine($"The ICECREAM is Eatable");
        }
        public override void Cost()
        {
            double i = 5;
            Console.WriteLine($"The ICECREAM costs {i} BYN");
        }

        public override void ShowCheapest()
        {
            Console.WriteLine();
        }

        public override void ShowExpensive()
        {
            Console.WriteLine();
        }
    }
    class Pencil : Item
    {
        public override void Characteristic()
        {
            Console.WriteLine($"The PENCIL is UnEatable");
        }
        public override void Cost()
        {
            double p = 2;
            Console.WriteLine($"The PENCIL costs {p} BYN");
        }

        public override void ShowCheapest()
        {
            Console.WriteLine("Pencil is the cheapest Item"); ;
        }

        public override void ShowExpensive()
        {
            Console.WriteLine();
        }
    }
    class Watch : Item
    {
        public override void Characteristic()
        {
            Console.WriteLine($"The WATCH is UnEatable");
        }

        public override void Cost()
        {
            double w = 40;
            Console.WriteLine($"The WATCH costs {w} BYN");
        }
        public override void ShowExpensive()
        {
            Console.WriteLine("Watch is the most expensive Item");
        }
        public override void ShowCheapest()
        {
            Console.WriteLine();
        }
    }
       
   
    class Program
    {
        static void Main(string[] args)
        {
            Shop shop = new Shop();
            Item[] range = { new Meat(), new IceCream(), new Pencil(), new Watch()};

            foreach (var item in range)
            {
                shop.Cost(item);
                shop.Characteristic(item);
                shop.ShowCheapest(item);
                shop.ShowExpensive(item);
                Console.WriteLine();
            }
        }
    }
}
